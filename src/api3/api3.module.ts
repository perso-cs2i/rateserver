import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiController } from './api/api.controller';
import { Rate3ServiceService } from './rate3-service/rate3-service.service';
import { Rate3Schema } from './rate3.schema';


@Module({
  imports: [
  MongooseModule.forFeature([
      {name: 'Rate3', schema: Rate3Schema}
  ])
  ],
  providers: [Rate3ServiceService],
  controllers: [ApiController]
})
export class Api3Module {}
