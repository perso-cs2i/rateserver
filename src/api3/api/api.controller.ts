import { Controller, Get, Param } from '@nestjs/common';
import { Rate3ServiceService } from '../rate3-service/rate3-service.service';

@Controller('api/3')
export class ApiController {

    constructor(private rateService: Rate3ServiceService){}

    @Get('rates')
    rates()
    {
        return this.rateService.getRates()
    }

    @Get('currencies')
    currencies()
    {
        return this.rateService.getCurrencies()
    }

    /*@Get('rates/:code')
    findOne()
    {
        return this.rateService.findOne("USD")
    }*/

    @Get('rates/:unCode')
    findOne2(@Param('unCode') unCode: string)
    {
        return this.rateService.findOne2(unCode)
    }


    /*@Get('rates/convert/:unCode')
    convert(@Param('unCode') unCode: string)
    {
        return this.rateService.convert(unCode)
    }*/

    @Get('rates/convert/:code')
    convert2()
    {
        return this.rateService.convert2("EUR")
    }

    // pour le controller : (@Param('code') code: string)
}

