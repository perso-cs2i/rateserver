import { Schema } from "mongoose";

export const Rate3Schema = new Schema({
    code: String,
    name: String,
    value: Number
}, {collection: 'rates3'})