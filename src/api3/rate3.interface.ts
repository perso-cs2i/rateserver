import { Document } from "mongoose";

export interface Rate3 extends Document{
    code: string
    name: string
    value: number
}

