import { Test, TestingModule } from '@nestjs/testing';
import { Rate3ServiceService } from './rate3-service.service';

describe('Rate3ServiceService', () => {
  let service: Rate3ServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Rate3ServiceService],
    }).compile();

    service = module.get<Rate3ServiceService>(Rate3ServiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
