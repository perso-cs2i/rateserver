import { DefaultValuePipe, Injectable, Param } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Rate3 } from '../rate3.interface';

@Injectable()
export class Rate3ServiceService {

    constructor(
        @InjectModel('Rate3')
        private rateModel: Model<Rate3>) {

    }

    getRates() {
        const rates =
            this.rateModel.find(
                { },
                {_id: false}
            )
        return rates
    }


    getCurrencies() {
        const currencies = 
            this.rateModel.find(
                { },
                {_id: false, value: false}
            )
        return currencies
    }

    
    /*findOne(@Param('code') code: string) {
        const rate = 
            this.rateModel.findOne(
                { code },
                {_id: false}
            )
        return rate

    }*/

    findOne2(unCode: string) {
        const rate = 
            this.rateModel.find(
                { "code": unCode },
                {_id: false}
            )
        return rate

    }

    convert(unCode: string) {
        const euro = 0.881483
        const rateConvert = 
            this.rateModel.find(
                { "code": unCode},
                
                {_id: false}
            )
        return rateConvert
    }

    convert2(@Param('code') code: string) {
        
        //const euro = 0.881483
        const rate = 
            this.rateModel.aggregate([
                {$project: 
                    {value: {$multiply : ["$value", 0.881483] }, code: true, _id: false } 
                }
            ])
        return rate

    }

}
