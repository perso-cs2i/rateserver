import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Currency } from '../currency.interface';

@Injectable()
export class CurrencyService {

    constructor(
        @InjectModel('Currency')
        private currencyModel: Model<Currency>) {

    }

    getCurrencies() {
        const currencies =
            this.currencyModel.find(
                { },
                {_id: false, __v: false}
            )
        return currencies
    }
}
