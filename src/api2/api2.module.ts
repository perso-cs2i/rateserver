import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CurrencySchema } from './currency.schema';
import { RateSchema } from './rate.schema';
import { CurrencyService } from './currency/currency.service';
import { ApiController } from './api/api.controller';

@Module({
    imports: [
    // name donne le nom de chaque modèle 
    MongooseModule.forFeature([
        {name: 'Currency', schema: CurrencySchema},
        {name: 'Rate', schema: RateSchema}
    ])
    ],
    providers: [CurrencyService],
    controllers: [ApiController]
})
export class Api2Module {}
