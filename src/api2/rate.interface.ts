import { Document } from "mongoose";

export interface Rate extends Document{
    code: string
    value: number
}
