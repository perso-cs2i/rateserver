import { Schema } from "mongoose";

export const CurrencySchema = new Schema({
    code: String,
    name: String
})