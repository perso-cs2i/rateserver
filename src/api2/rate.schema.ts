import { Schema } from "mongoose";

export const RateSchema = new Schema({
    code: String,
    value: Number
})
