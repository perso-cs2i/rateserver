import { Controller, Get } from '@nestjs/common';
import { CurrencyService } from '../currency/currency.service';

@Controller('api/2')
export class ApiController {

    constructor(private currencyService: CurrencyService){}

    @Get('currencies')
    currencies()
    {
        return this.currencyService.getCurrencies()
    }

}
