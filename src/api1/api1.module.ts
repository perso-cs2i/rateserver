import { Module } from '@nestjs/common';
import { ApiControllerController } from './api-controller/api-controller.controller';

@Module({
  controllers: [ApiControllerController]
})
export class Api1Module {}
