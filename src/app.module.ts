import { Module } from '@nestjs/common';
import { WebModule } from './web/web.module';
import { Api1Module } from './api1/api1.module';
import { Api2Module } from './api2/api2.module';
import { DefaultController } from './web/default/default.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Api3Module } from './api3/api3.module';

//Windows
const MONGO_CONNECTION = "mongodb://localhost:27017/rate"

@Module({
  imports: [
    WebModule,
    Api1Module,
    Api2Module,Api3Module,

    MongooseModule.forRoot(MONGO_CONNECTION,{useNewUrlParser: true, useUnifiedTopology: true}
      ),
    
    ]
})
export class AppModule {}
