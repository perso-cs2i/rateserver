import { Module } from '@nestjs/common';
import { DefaultController } from './default/default.controller';

@Module({
  controllers: [DefaultController]
})
export class WebModule {}
