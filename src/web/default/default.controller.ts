import { Controller, Get, Render } from '@nestjs/common';

/**
 * Controleur du module web
 * L'annotation @Controller() peut contenir une chaine
 * Ex. : @Controller('default')
 */
@Controller()
export class DefaultController {

    /**
     * Page d'accueil
     * L'annotation @Get() peut contenur une chaine
     * Ex. : @Get('test')
     * L'URL pour accéder à cette méthode est la combinaison
     * des chaînes du contrôleur et de @Get()
     * Ex. : /default/test 
     * @returns 
     */
    @Get()
    @Render('home')
    home() {
        return {
            title: "NestRates",
            subtitle: "Welcome to NestRates!",
            apiversion: 1
        }
    }

    @Get('/rates')
    @Render('rates')
    rates() {
        return {
            title: "Rates",
            subtitle: "API version 1",
            apiversion: 1
        }
    }

    @Get('/currencies')
    @Render('currencies')
    currencies() {
        return {
            title: "Currencies",
            subtitle: "API version 1",
            apiversion: 1
        }
    }
}
